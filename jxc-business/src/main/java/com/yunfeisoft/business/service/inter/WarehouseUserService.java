package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.WarehouseUser;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: WarehouseUserService
 * Description: 仓库用户信息service接口
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseUserService extends BaseService<WarehouseUser, String> {

    public Page<WarehouseUser> queryPage(Map<String, Object> params);
}