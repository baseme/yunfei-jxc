package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.SupplierDao;
import com.yunfeisoft.business.model.Supplier;
import com.yunfeisoft.business.service.inter.SupplierService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierServiceImpl
 * Description: 供应商信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("supplierService")
public class SupplierServiceImpl extends BaseServiceImpl<Supplier, String, SupplierDao> implements SupplierService {

    @Override
    @DataSourceChange(slave = true)
    public Page<Supplier> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<Supplier> queryList(Map<String, Object> params) {
        return getDao().queryList(params);
    }

    @Override
    public List<Supplier> queryByName(String orgId, String name) {
        return getDao().queryByName(orgId, name);
    }

    @Override
    public boolean isDupName(String orgId, String id, String name) {
        return getDao().isDupName(orgId, id, name);
    }
}