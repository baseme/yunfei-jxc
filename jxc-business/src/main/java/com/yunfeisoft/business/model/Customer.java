package com.yunfeisoft.business.model;

import com.alibaba.excel.annotation.ExcelProperty;
import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * ClassName: Customer
 * Description: 客户信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_CUSTOMER")
public class Customer extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 编码
     */
    @ExcelProperty("编码")
    @Column
    private String code;

    /**
     * 名称
     */
    @ExcelProperty("名称")
    @Column
    private String name;

    /**
     * 联系人
     */
    @ExcelProperty("联系人")
    @Column
    private String linkman;

    /**
     * 类型(1零售客户,2会员客户,3批发客户)
     */
    @Column
    private Integer type;

    /**
     * 传真
     */
    @ExcelProperty("传真")
    @Column
    private String fax;

    /**
     * 会员卡号
     */
    @ExcelProperty("会员卡号")
    @Column
    private String memberNo;

    /**
     * 生日
     */
    @ExcelProperty("生日")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date birthDate;

    /**
     * 常用电话
     */
    @ExcelProperty("常用电话")
    @Column
    private String phone;

    /**
     * 备用电话
     */
    @ExcelProperty("备用电话")
    @Column
    private String standbyPhone;

    /**
     * 地址
     */
    @ExcelProperty("地址")
    @Column
    private String address;

    /**
     * QQ号
     */
    @ExcelProperty("QQ号")
    @Column
    private String qq;

    /**
     * 微信
     */
    @ExcelProperty("微信")
    @Column
    private String wechat;

    /**
     * 旺旺
     */
    @ExcelProperty("旺旺")
    @Column
    private String wangwang;

    /**
     * Email
     */
    @ExcelProperty("Email")
    @Column
    private String email;

    /**
     * 总销售金额
     */
    @ExcelProperty("总销售金额")
    @Column
    private BigDecimal balance;

    /**
     * 拼音简码
     */
    @ExcelProperty("拼音简码")
    @Column
    private String pinyinCode;

    /**
     * 备注
     */
    @ExcelProperty("备注")
    @Column
    private String remark;

    @ExcelProperty("类型")
    @TransientField
    private String typeStr;

    public String getTypeStr() {
        if (StringUtils.isNotBlank(typeStr)) {
            return typeStr;
        }
        return CustomerTypeEnum.valueOf(type);
    }

    public void setTypeStr(String typeStr) {
        this.typeStr = typeStr;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStandbyPhone() {
        return standbyPhone;
    }

    public void setStandbyPhone(String standbyPhone) {
        this.standbyPhone = standbyPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getWangwang() {
        return wangwang;
    }

    public void setWangwang(String wangwang) {
        this.wangwang = wangwang;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getPinyinCode() {
        return pinyinCode;
    }

    public void setPinyinCode(String pinyinCode) {
        this.pinyinCode = pinyinCode;
    }

    /**
     * 类型(1零售客户,2会员客户,3批发客户)
     */
    public enum CustomerTypeEnum {

        RETAIL(1, "零售客户"),
        MEMBER(2, "会员客户"),
        WHOLESALE(2, "批发客户");

        private int value;
        private String label;

        private CustomerTypeEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (CustomerTypeEnum loop : CustomerTypeEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

}