package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * ClassName: SaleOrder
 * Description: 销售单信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_SALE_ORDER")
public class SaleOrder extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 客户id
     */
    @Column
    private String customerId;

    /**
     * 订单编号
     */
    @Column
    private String code;

    /**
     * 销售日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date saleDate;

    /**
     * 总金额
     */
    @Column
    private BigDecimal totalAmount;

    /**
     * 总成本
     */
    @Column
    private BigDecimal totalCostAmount;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 状态(1待出库，2已出库，3待更改)
     */
    @Column
    private Integer status;

    /**
     * 收款状态(1待收款，2已收款，3待收清)
     */
    @Column
    private Integer payStatus;

    /**
     * 已收金额
     */
    @Column
    private BigDecimal payAmount;

    @TransientField
    private String customerName;
    private List<SaleItem> saleItemList;
    private List<IncomeRecord> incomeRecordList;
    private IncomeRecord incomeRecord;
    private Customer customer;
    private String orgName;

    /**
     * 待收金额
     * @return
     */
    public BigDecimal getSurplusAmount() {
            return totalAmount.subtract(payAmount);
    }

    public String getStatusStr() {
        return SaleOrderStatusEnum.valueOf(status);
    }

    public String getPayStatusStr() {
        return SaleOrderPayStatusEnum.valueOf(payStatus);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public List<SaleItem> getSaleItemList() {
        return saleItemList;
    }

    public void setSaleItemList(List<SaleItem> saleItemList) {
        this.saleItemList = saleItemList;
    }

    public IncomeRecord getIncomeRecord() {
        return incomeRecord;
    }

    public void setIncomeRecord(IncomeRecord incomeRecord) {
        this.incomeRecord = incomeRecord;
    }

    public List<IncomeRecord> getIncomeRecordList() {
        return incomeRecordList;
    }

    public void setIncomeRecordList(List<IncomeRecord> incomeRecordList) {
        this.incomeRecordList = incomeRecordList;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getTotalCostAmount() {
        return totalCostAmount;
    }

    public void setTotalCostAmount(BigDecimal totalCostAmount) {
        this.totalCostAmount = totalCostAmount;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 状态(1待出库，2已出库，3待更改)
     */
    public enum SaleOrderStatusEnum {

        TO_BE_DELIVERED(1, "待出库"),
        DELIVERED(2, "已出库"),
        TO_BE_CHANGED(3, "待变更");

        private int value;
        private String label;

        private SaleOrderStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (SaleOrderStatusEnum loop : SaleOrderStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    /**
     * 收款状态(1待收款，2已收款，3待收清)
     */
    public enum SaleOrderPayStatusEnum {

        //TO_BE_RECEIVED(1, "待收款"),
        RECEIVED(2, "已结清"),
        TO_BE_CLEARED(3, "欠款");

        private int value;
        private String label;

        private SaleOrderPayStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (SaleOrderPayStatusEnum loop : SaleOrderPayStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}