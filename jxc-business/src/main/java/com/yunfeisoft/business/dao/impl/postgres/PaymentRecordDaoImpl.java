package com.yunfeisoft.business.dao.impl.postgres;

import com.applet.base.ServiceDaoImpl;
import com.applet.sql.builder.SelectBuilder;
import com.applet.sql.builder.WhereBuilder;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PaymentRecordDao;
import com.yunfeisoft.business.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: PaymentRecordDaoImpl
 * Description: 付款信息Dao实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Repository
public class PaymentRecordDaoImpl extends ServiceDaoImpl<PaymentRecord, String> implements PaymentRecordDao {

    @Override
    public Page<PaymentRecord> queryPage(Map<String, Object> params) {
        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("pr.createTime");
        if (params != null) {
            initPageParam(wb, params);
            wb.andEquals("pr.orgId", params.get("orgId"));
            wb.andFullLike("s.name", params.get("supplierName"));
        }

        SelectBuilder builder = getSelectBuilder("pr");
        builder.column("s.name as supplierName")
                .column("po.code as orderCode")
                .join(Supplier.class).alias("s").on("pr.supplierId = s.id").build()
                .join(PurchaseOrder.class).alias("po").on("pr.purchaseOrderId = po.id").build();

        return queryPage(builder.getSql(), wb);
    }

    @Override
    public List<PaymentRecord> queryByPurchaseOrderId(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            return new ArrayList<>();
        }

        WhereBuilder wb = new WhereBuilder();
        wb.setOrderByWithDesc("createTime");
        wb.andEquals("purchaseOrderId", purchaseOrderId);
        return query(wb);
    }

    @Override
    public int removeByPurchaseOrderId(String purchaseOrderId) {
        if (StringUtils.isBlank(purchaseOrderId)) {
            return 0;
        }

        WhereBuilder wb = new WhereBuilder();
        wb.andEquals("purchaseOrderId", purchaseOrderId);
        return deleteByCondition(wb);
    }
}