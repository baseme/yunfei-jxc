package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.CodeBuilder;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: CodeBuilderDao
 * Description: 编码生成记录Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface CodeBuilderDao extends BaseDao<CodeBuilder, String> {

    public Page<CodeBuilder> queryPage(Map<String, Object> params);

    public List<CodeBuilder> queryByName(String name, String orgId);
}