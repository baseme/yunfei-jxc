package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.IncomeRecord;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: IncomeRecordDao
 * Description: 收款信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface IncomeRecordDao extends BaseDao<IncomeRecord, String> {

    public Page<IncomeRecord> queryPage(Map<String, Object> params);

    public List<IncomeRecord> queryBySaleOrderId(String saleOrderId);

    public int removeBySaleOrderId(String saleOrderId);
}