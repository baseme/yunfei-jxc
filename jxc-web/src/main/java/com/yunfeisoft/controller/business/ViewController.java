package com.yunfeisoft.controller.business;

import com.applet.base.BaseModel;
import com.applet.session.DomainModel;
import com.applet.utils.AjaxUtils;
import com.applet.utils.WebUtils;
import com.yunfeisoft.utils.ApiUtils;
import com.yunfeisoft.utils.SysConfigCache;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jackie Liu on 2017/3/18.
 */
@Controller
public class ViewController {

    @Autowired
    private DomainModel domainModel;
    @Value("${file.request.url}")
    private String fileRequestUrl;
    @Autowired
    private SysConfigCache sysConfigCache;

    /**
     * js、css页面随机参数，防止缓存
     */
    private static final long PAGE_RANDOM = System.currentTimeMillis();

    @RequestMapping("/view/{dir1}/{dir2}/{page}")
    public String view(@PathVariable("dir1") String dir1, @PathVariable("dir2") String dir2, @PathVariable("page") String page, HttpServletRequest request) {
        if (StringUtils.isEmpty(dir1) || StringUtils.isEmpty(dir2)
                || StringUtils.isEmpty(page)) {
            return "404";
        }
        gainParams(request);
        return dir1 + "/" + dir2 + "/" + page;
    }

    @RequestMapping("/view/{dir}/{page}")
    public String view(@PathVariable("dir") String dir, @PathVariable("page") String page, HttpServletRequest request) {
        if (StringUtils.isEmpty(dir) || StringUtils.isEmpty(page)) {
            return "404";
        }
        gainParams(request);
        return dir + "/" + page;
    }

    @RequestMapping("/{page}")
    public String page(@PathVariable("page") String page, HttpServletRequest request) {
        if (StringUtils.isEmpty(page)) {
            return "404";
        }
        gainParams(request);
        return page;
    }

    private void gainParams(HttpServletRequest request) {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        WebUtils.gainParams(paramsMap);
        request.setAttribute("params", paramsMap);

        paramsMap.put("contextPath", domainModel.getWebDomain());
        paramsMap.put("fileRequestUrl", fileRequestUrl);
        paramsMap.put("pageRandom", PAGE_RANDOM);

        Map<String, String> configMap = sysConfigCache.getConfig();
        if (MapUtils.isNotEmpty(configMap)) {
            paramsMap.putAll(configMap);
        }

        BaseModel user = ApiUtils.getLogin();
        request.setAttribute("user", user);
    }

    @RequestMapping("/web/keepAlive")
    public void keepAlive(HttpServletRequest request, HttpServletResponse response) {
        AjaxUtils.ajaxJsonSuccessMessage("SUCCESS");
    }
}
